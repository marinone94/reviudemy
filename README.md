# reviudemy
Repository connected to the udemy courses: 
- https://udemy.com/courses/beginner (B)
- https://udemy.com/courses/intermediate (I)
- https://udemy.com/courses/advanced (A)

Product:
Web-app with a basic interface, where the user types the name of a udemy course and gets back the students rating and the reviews (B), a sentiment analysis of the reviews using Google Cloud Natural Language API (I) and the same analysis performed with state-of-the-art Machine Learning models and transfer learning. The app is deployed on Google Cloud using Google Kuebernetes Engine and GitLab CI.

---

General notes:
This course will not cover all the details of the whole technology stack. It would be a Computer Science degree rather than an online course. Instead, the intent is to help you learning by explanation and practice how to set a CI/CD pipeline using GitLab CI and implement, test and deploy your app on Google Cloud using Google Kubernetes Engine. 
By first writing things which do not work, you will learn how to implement them correctly and understand which are the underlying problems. Finally, you will learn some tricks to speed up the development, reduce the number of errors and write clean code. 
Hopefully, you won't need other sources to make it working. For detailed explanation and documentation of the stack, please refer to the linked sources, the official documentation and your own exploration capabilites. 
GitLab offers one month of Gold Account for free, which will be used in the Intermediate and Advanced courses. Google Cloud Platform offers 300$ of free trial, but you will need to set a billing account with a Credit card to use it. In addition, you will get other 200$ of free trial from GitLab to be used on Google Cloud Platform. It will be enough to complete the courses and play further with those technologies. 

---

Beginner:
You will explore how to go from an idea to implementing and deploying a production web-app using Visual Studio Code, Git, GitLab, Docker, Kubernetes, Google Kubernetes Engine, Python, NodeJS, Udemy API and MongoDB. You will also learn what are Continuous Integration, Continuous Delivery and Continuous Deployment, as how to build automatic pipelines using GitLab CI.

In the first chapter, you will get an introduction of the above-mentioned technologies, you will set up the environments and the required packages and accounts.
Later, you will implement the different services and write the files required to automatically build, test and deploy your application (.gitlab-ci.yml, Dockerfiles and Kubernetes manifests).
In conclusion, you will get a preview of the topics covered in the intermediate course.

This course requires you to have a basic knowledge of Git and Python.

---

Intermediate:
Starting from the app built in the Beginner course, you will set HTTPS connections and create your own domain, connect to the Google Cloud NL API, write unit and integration tests, set testing, staging and production environments on Kubernetes and the related pipeline. You will use Istio to manage the connection between Pods and with the Internet, to monitor your services and to secure your app. The course follows the same structure as the Beginner course.

In the first chapter, you will get an introduction of the above-mentioned technologies, you will set up the environments and the required packages and accounts.
Later, you will implement the different services and write the files required to automatically build, test and deploy your application (.gitlab-ci.yml, Dockerfiles and Kubernetes manifests).
In conclusion, you will get a preview of the topics covered in the intermediate course.

This course requires you to know Docker, Kubernetes, Git and Python.

---

Advanced:
Starting from the app built in the Intermediate course, you will build, train, deploy and monitor a state-of-the-art model and integrate them in a CI/CD pipeline. The course follows the same structure as the Beginner and Intermediate courses. 

In the first chapter, you will get an introduction of the above-mentioned technologies and you will set up the environments and the required packages.
Later, you will implement the different services and write the files required to automatically build, test and deploy your application (.gitlab-ci.yml, Dockerfiles and Kubernetes manifests).
In conclusion, you will get an overview of some other models, cloud services and frameworks that could have been used instead.